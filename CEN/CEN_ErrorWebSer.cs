﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class CEN_ErrorWebSer
    {
        //DESCRIPCION : CLASE DE ERROR
        public short TipoErr { get; set; }          // tipo de error 
        public int CodigoErr { get; set; }          // codigo de error 
        public string DescripcionErr { get; set; }  // descripcion de error 
        public CEN_ErrorWebSer()
        {
            TipoErr = CEN_Constante.g_const_0;
            CodigoErr = CEN_Constante.g_const_0;
            DescripcionErr = CEN_Constante.g_const_vacio;
        }
    }
}
