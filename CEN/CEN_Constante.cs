﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class CEN_Constante
    {
        public const string g_const_vacio = ""; // vacio
        public const int g_const_10 = 10; //Constante 10
        public const int g_const_20 = 20; //Constante 20
        public const int g_const_2 = 2; //Constante 2
        public const string g_const_urlServicioSunat = "urlServiceSunat";
        public const string g_const_urlServicioReniec = "urlServiceReniec";
        public const string g_const_apliJSON = "application/json"; // Constante application/json
        public const string g_const_POST = "POST"; // Constante POST
        public const int g_const_1 = 1; //Constante 1
        public const int g_const_0 = 0; //Constante 0
        public const int g_const_2000 = 2000; //Constante 2000
        public const int g_const_3000 = 3000; //Constante 3000
        public const string g_const_espacio = " "; //espacio
        public const string g_const_consultaexitosa = "Consulta exitosa"; //constate consulta exitosa
    }
}
