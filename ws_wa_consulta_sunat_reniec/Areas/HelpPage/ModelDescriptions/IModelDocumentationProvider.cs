using System;
using System.Reflection;

namespace ws_wa_consulta_sunat_reniec.Areas.HelpPage.ModelDescriptions
{
    public interface IModelDocumentationProvider
    {
        string GetDocumentation(MemberInfo member);

        string GetDocumentation(Type type);
    }
}