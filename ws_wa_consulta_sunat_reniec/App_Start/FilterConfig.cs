﻿using System.Web;
using System.Web.Mvc;

namespace ws_wa_consulta_sunat_reniec
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
