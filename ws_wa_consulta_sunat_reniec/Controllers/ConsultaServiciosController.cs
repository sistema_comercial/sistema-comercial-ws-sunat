﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CEN;
using CLN;

namespace ws_wa_consulta_sunat_reniec.Controllers
{
    public class ConsultaServiciosController : ApiController
    {
        [HttpPost]
        [ActionName("ConsultaDocumentoSunat")]
        public IHttpActionResult ConsultaDocumentoSunat(CEN_RequestSunat request)
        {
            IHttpActionResult result;
            CLN_Sunat cln_sunat = new CLN_Sunat();
            CEN_RespuestaWSSunat respuestaWS = new CEN_RespuestaWSSunat();
            try
            {
                respuestaWS = cln_sunat.ConsultaDocumentoSunat(request);
                result = Ok(respuestaWS);
                return result;
            }
            catch (Exception ex)
            {
                respuestaWS.ErrorWebSer.TipoErr = CEN_Constante.g_const_1;
                respuestaWS.ErrorWebSer.CodigoErr = CEN_Constante.g_const_3000;
                respuestaWS.ErrorWebSer.DescripcionErr = ex.Message + CEN_Constante.g_const_espacio + ex.StackTrace;
                result = Content(HttpStatusCode.BadRequest, respuestaWS);
                return result;
            }
        }

        [HttpPost]
        [ActionName("ConsultaDocumentoReniec")]
        public IHttpActionResult ConsultaDocumentoReniec(CEN_RequestReniec request)
        {
            IHttpActionResult result;
            CLN_Reniec cln_reniec = new CLN_Reniec();
            CEN_RespuestaWSReniec respuestaWS = new CEN_RespuestaWSReniec();
            try
            {
                respuestaWS = cln_reniec.ConsultaDocumentoReniec(request);
                result = Ok(respuestaWS);
            }
            catch (Exception ex)
            {
                respuestaWS.ErrorWebSer.TipoErr = CEN_Constante.g_const_1;
                respuestaWS.ErrorWebSer.CodigoErr = CEN_Constante.g_const_3000;
                respuestaWS.ErrorWebSer.DescripcionErr = ex.Message + CEN_Constante.g_const_espacio + ex.StackTrace;
                result = Content(HttpStatusCode.BadRequest, respuestaWS);
            }
            return result;
        }
    }
}
