﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CEN;
using Newtonsoft.Json;

namespace CLN
{
    public class CLN_ConexionServicio
    {
        public CEN_ResponseServ_RUC ConsultaDocumentoSunat(CEN_RequestSunat data)
        {
            //DESCRIPCIÓN: Verificar registro de términos y condiciones
            CLN_Consulta consulta = new CLN_Consulta();
            Dictionary<string, RepresentaLegal> representante_legal;
            Dictionary<string, Empleado> empleados;
            CEN_ResponseServ_RUC ruc = new CEN_ResponseServ_RUC();
            try
            {
                // Definir el URL de la aplicación Web API
                string URLWebAPI = ConfigurationManager.ConnectionStrings[CEN_Constante.g_const_urlServicioSunat].ConnectionString.ToString();
                string endpoint = URLWebAPI + data.numDocumento;

                HttpWebRequest request = WebRequest.Create(endpoint) as HttpWebRequest;
                request.Method = CEN_Constante.g_const_POST;
                request.ContentType = CEN_Constante.g_const_apliJSON;

                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                StreamReader reader = new StreamReader(response.GetResponseStream());
                string json = reader.ReadToEnd();
               
                ruc = JsonConvert.DeserializeObject<CEN_ResponseServ_RUC>(json);
                int valRuc = consulta.verificarTipoPersonaRuc(ruc.ruc);
                if(valRuc == CEN_Constante.g_const_2)
                {
                    //PERSONA JURIDICA
                    string data_1 = Convert.ToString(ruc.representante_legal);
                    representante_legal = JsonConvert.DeserializeObject<Dictionary<string, RepresentaLegal>>(data_1);

                    string data_2 = Convert.ToString(ruc.empleados);
                    empleados = JsonConvert.DeserializeObject<Dictionary<string, Empleado>>(data_2);
                    //Guardar en base de datos


                }
                //CEN_R1 ruc = JsonConvert.DeserializeObject<CEN_R1>(json);
                return ruc;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CEN_RespuestaWSConsulRec ConsultaDocumentoReniec(CEN_RequestReniec data)
        {
            //DESCRIPCIÓN: Verificar registro de términos y condiciones
            CLN_Consulta consulta = new CLN_Consulta();
            CEN_RespuestaWSConsulRec respuesta = new CEN_RespuestaWSConsulRec();
            try
            {
                // Definir el URL de la aplicación Web API
                string URLWebAPI = ConfigurationManager.ConnectionStrings[CEN_Constante.g_const_urlServicioReniec].ConnectionString.ToString();
                string endpoint = URLWebAPI + data.numDocumento;

                HttpWebRequest request = WebRequest.Create(endpoint) as HttpWebRequest;
                request.Method = CEN_Constante.g_const_POST;
                request.ContentType = CEN_Constante.g_const_apliJSON;

                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                StreamReader reader = new StreamReader(response.GetResponseStream());
                string json = reader.ReadToEnd();

                respuesta = JsonConvert.DeserializeObject<CEN_RespuestaWSConsulRec>(json);
                
                //CEN_R1 ruc = JsonConvert.DeserializeObject<CEN_R1>(json);
                return respuesta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
