﻿using CEN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLN
{
    public class CLN_Consulta
    {
        public int verificarTipoPersonaRuc(string numDocumento)
        {
            try
            {
                if(numDocumento != null)
                {
                    string codNum = numDocumento.Substring(0, 2);
                    if(codNum == CEN_Constante.g_const_10.ToString())
                    {
                        return 1;
                    }
                    else
                    {
                        return 2;
                    }
                }
                else
                {
                    return -1;
                }
            }catch(Exception ex)
            {
                throw ex;
            }
        }
        public string ValidarCadenaNombre(string cadena)
        {
            try
            {
                cadena = cadena.Replace("&Ntilde;", "Ñ");
                cadena = cadena.Replace("&Aacute;", "Á");
                cadena = cadena.Replace("&Eacute;", "É");
                cadena = cadena.Replace("&Iacute;", "Í");
                cadena = cadena.Replace("&Oacute;", "O");
                cadena = cadena.Replace("&Uacute;", "U");
                return cadena;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
