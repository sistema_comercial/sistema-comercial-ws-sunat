﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CEN;

namespace CLN
{
    public class CLN_Sunat
    {
        public CEN_RespuestaWSSunat ConsultaDocumentoSunat(CEN_RequestSunat request)
        {
            CLN_ConexionServicio servicio = new CLN_ConexionServicio();
            CEN_ResponseServ_RUC respuestaRuc = new CEN_ResponseServ_RUC();
            CEN_RespuestaWSSunat respuestaServ = new CEN_RespuestaWSSunat();
            try
            {
                respuestaRuc = servicio.ConsultaDocumentoSunat(request);
                respuestaServ = LLenarDataMetodo(respuestaRuc);
                respuestaServ.ErrorWebSer.TipoErr = CEN_Constante.g_const_0;
                respuestaServ.ErrorWebSer.CodigoErr = CEN_Constante.g_const_2000;
                respuestaServ.ErrorWebSer.DescripcionErr = CEN_Constante.g_const_consultaexitosa;
                return respuestaServ;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private CEN_RespuestaWSSunat LLenarDataMetodo(CEN_ResponseServ_RUC respuestaRuc)
        {
            CEN_RespuestaWSSunat data = new CEN_RespuestaWSSunat();
            CLN_Consulta cln_consulta = new CLN_Consulta();
            try
            {
                if(respuestaRuc != null)
                {
                    data.respuestaWsSunat.actividad_exterior = respuestaRuc.actividad_exterior;
                    data.respuestaWsSunat.ciiu = respuestaRuc.ciiu;
                    data.respuestaWsSunat.contribuyente_condicion = respuestaRuc.contribuyente_condicion;
                    data.respuestaWsSunat.contribuyente_estado = respuestaRuc.contribuyente_estado;
                    data.respuestaWsSunat.contribuyente_tipo = respuestaRuc.contribuyente_tipo;
                    data.respuestaWsSunat.domicilio_fiscal = respuestaRuc.domicilio_fiscal;
                    data.respuestaWsSunat.emision_electronica = respuestaRuc.emision_electronica;
                    data.respuestaWsSunat.fecha_actividad = respuestaRuc.fecha_actividad;
                    data.respuestaWsSunat.fecha_baja = respuestaRuc.fecha_baja;
                    data.respuestaWsSunat.fecha_inscripcion = respuestaRuc.fecha_inscripcion;
                    data.respuestaWsSunat.fecha_inscripcion_ple = respuestaRuc.fecha_inscripcion_ple;
                    data.respuestaWsSunat.nombre_comercial = respuestaRuc.nombre_comercial;
                    if(respuestaRuc.Oficio == null)
                    {
                        data.respuestaWsSunat.Oficio = CEN_Constante.g_const_vacio;
                    }
                    else
                    {
                        data.respuestaWsSunat.Oficio = respuestaRuc.Oficio;
                    }

                    data.respuestaWsSunat.razon_social = cln_consulta.ValidarCadenaNombre(respuestaRuc.razon_social);
                    data.respuestaWsSunat.ruc = respuestaRuc.ruc;
                    data.respuestaWsSunat.sistema_contabilidad = respuestaRuc.sistema_contabilidad;
                    data.respuestaWsSunat.sistema_emision = respuestaRuc.sistema_emision;

                }
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
