﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CEN;

namespace CLN
{
    public class CLN_Reniec
    {
        public CEN_RespuestaWSReniec ConsultaDocumentoReniec(CEN_RequestReniec request)
        {
            CEN_RespuestaWSConsulRec respuestaWS = new CEN_RespuestaWSConsulRec();
            CEN_RespuestaWSReniec respuestaServ = new CEN_RespuestaWSReniec();
            CLN_ConexionServicio servicio = new CLN_ConexionServicio();
            try
            {
                respuestaWS = servicio.ConsultaDocumentoReniec(request);
                respuestaServ = LlenarDataMetodo(respuestaWS);
                respuestaServ.ErrorWebSer.TipoErr = CEN_Constante.g_const_0;
                respuestaServ.ErrorWebSer.CodigoErr = CEN_Constante.g_const_2000;
                respuestaServ.ErrorWebSer.DescripcionErr = CEN_Constante.g_const_consultaexitosa;
                return respuestaServ;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        private CEN_RespuestaWSReniec LlenarDataMetodo(CEN_RespuestaWSConsulRec respuestaWS)
        {
            CEN_RespuestaWSReniec data = new CEN_RespuestaWSReniec();
            CLN_Consulta cln_consulta = new CLN_Consulta();
            try
            {
                data.respuestaWsReniec.apellido_materno = cln_consulta.ValidarCadenaNombre(respuestaWS.apellido_materno);
                data.respuestaWsReniec.apellido_paterno = cln_consulta.ValidarCadenaNombre(respuestaWS.apellido_paterno);
                data.respuestaWsReniec.cui = respuestaWS.cui;
                data.respuestaWsReniec.dni = respuestaWS.dni;
                data.respuestaWsReniec.nombres = cln_consulta.ValidarCadenaNombre(respuestaWS.nombres);
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
